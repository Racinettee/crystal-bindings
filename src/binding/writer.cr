require "./nodes"
require "./parse"
module Bindings
    class WriteOuter
        def initialize(@module_name : String, @remove_prefix="", @module_type="lib")
            @global = NodeGlobal.new
            @lib = NodeLib.new(@module_name)
        end
        def add(node : Node) : Void
            case node
            when NodeMacro, NodeConst, NodeInclude
                @global.add(node)
            else
                @lib.add(node)
            end
        end
        def gen_file(context) : Void
            filename = context["filename"].as(String)
            puts "Opening file: #{filename}"
            outfile = Util.rm_prefix(context["rm_prefix"].as(String), File.basename(filename))
            outfile = context["outdir"].as(String) + "/" + outfile
            outfile = outfile[0..-3] + ".cr"
            puts "Output filename: #{outfile}"
            Parser.parse_file(filename, self)
            write(context, outfile)
        end
        private def write(context, outfile)
            context["rm_prefix"] = @remove_prefix
            output = File.new(outfile, "w")
            output.puts "# AUTOMATICALLY GENERATED, DO NOT EDIT"
            @global.emit(context, output)
            @lib.emit(context, output)
            output.close
        end
    end
end