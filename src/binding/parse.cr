require "./nodes"

module Bindings
  class Parser
    COMMENT_RGX = /\/\*|\s?\*/
    @@argn = 0

    def self.read_full_decl(line : String, input : File) : String
      out_line = line
      until out_line.index(";")
        out_line += input.read_line()
      end
      return out_line
    end
    def self.shift_star(line : String) : String
      return line.squeeze(' ').gsub(/\s?\*\s?/, "* ")
    end
    def self.rm_line_comment(line : String) : String
      return line.gsub(/(\/\*|\/\/).*/, "")
    end
    def self.parse_arg(arg : String) : NodeVariable
      parts = arg.split(/\s/)
      # delete any part with "const"
      parts.delete("")
      parts.delete("const")
      # the type is for sure required
      return NodeVariable.new(Node.conv_type(parts[0]), parts[1]? || "arg#{@@argn+=1}")
    end
    def self.parse_args(func_name : String, line : String, input : File) : Array(NodeVariable)
      # Find the occurence of the '(' character following the function name
      find_tok = "#{func_name}("
      if index = line.index(find_tok)
        index += find_tok.size

        line = Parser.shift_star(Parser.read_full_decl(line, input).gsub(/\s/, " "))
            
        # Return right away, because the next character is the close paren
        return [] of NodeVariable if line[index] == ")"

        index_end = line.index(");") || -1
        index_end -= 1 if index_end != -1

        args_str = line[index..index_end]

        # Many libraries in C provide just "void" to indicate no args
        return [] of NodeVariable if args_str == "void"
  
        args = args_str.split(/\s?,\s?/)

        output = [] of NodeVariable

        if args.size > 0
          args.each do |arg|
            output << Parser.parse_arg(arg)
          end
        else
          typ, name = args_str.split(/\s/)
          output << NodeVariable.new(typ, name)
        end
        return output
      end
      return [] of NodeVariable
    end
    def self.parse_include(matches : Regex::MatchData, input : File) : NodeInclude
      bracket_style = matches[1]
      path = matches[2]
      return NodeInclude.new(path, bracket_style)
    end
    def self.parse_macro_or_const(matches : Regex::MatchData, input : File) : Node
      value = matches[3].lstrip()
      while value.ends_with?("\\")
        value = value[0..-2] + input.read_line().lstrip()
      end
      name = matches[1]
      if args = matches[2]?
        return NodeMacro.new(name, args, value)
      else
        return NodeConst.new(name, value)
      end
    end
    def self.parse_function(matches : Regex::MatchData, line : String, input : File) : NodeFunction
      typ = matches[1] || matches[2]
      api = matches[2] if matches[1]
      name = matches[3]
      if (api)
        if (api.starts_with?("*"))
          typ += "*"
          api = api[1..-1]
        end
      end
      args = Parser.parse_args(name, line, input)
      return NodeFunction.new(typ, name, args)
    end
    def self.parse_struct(matches : Regex::MatchData, input : File) : Node
      if matches[2]? && matches[3]?
        # This struct is defined to be used only as a pointer
        return NodeTypedef.new(matches[3], matches[2])
      end
      line = input.read_line().lstrip()
      if line == "{"
        line = input.read_line().lstrip()
      end
      members = [] of NodeVariable
      until line.includes?("}")
        line = self.shift_star(self.rm_line_comment(line))
        parts = line.split(' ')
        parts.delete("const")
        parts.delete("")
        members << NodeVariable.new(parts[0], parts[1][0..-2])
        line = input.read_line()
      end
      matches = /\}\s?([a-zA-Z0-9_]+);/.match(line)
      name = matches && matches[1] ? matches[1] : ""
      return NodeStruct.new(name, members)
    end
    def self.parse_typedef(matches : Regex::MatchData, line : String, input : File) : Node
      typedef = Parser.shift_star(Parser.read_full_decl(line, input))
      if matches = /([a-zA-Z0-9_]+)\s?\((?:[a-zA-Z0-9_]+)?\s?\*\s?([a-zA-Z0-9_]+)\)\s?\((.*)\);/.match(typedef)
        args = [] of NodeVariable
        rawargs = matches[3].split(',')
        rawargs.each do |arg|
          parts = arg.split(' ')
          parts.delete("const")
          parts.delete("")
          args << NodeVariable.new(parts[0], parts[1])
        end
        return NodeFunctionTypedef.new(matches[2], matches[1], args)
      end
      typedef = typedef[0..-2] if typedef.ends_with?(';')
      parts = typedef.split(' ')
      parts.delete("const")
      parts.delete("")
      return NodeTypedef.new(parts[2], parts[1])
    end
    def self.parse_enum(matches : Regex::MatchData, input : File) : NodeEnum
      const_nodes = [] of NodeConst
      # coming from parse_eos matches[1] is enum|struct
      puts "Working on enum"
      enum_name = matches[2]?
      line = input.read_line().lstrip()
      if line == "{"
        line = input.read_line().lstrip()
      end
      until line.includes?("}")
        if line.index(COMMENT_RGX) != 0
          const_matches = /^([A-Z0-9_]+)(?:\s*=\s*([a-zA-Z0-9_\(\)\|\&\<\>\*\/\+\-\ ]+))?/.match(line)
          if const_matches
            name = const_matches[1]
            if value = const_matches[2]?
              value = Parser.rm_line_comment(value)
            end
            const_nodes << NodeConst.new(name, value)
          end
        end
        line = input.read_line().lstrip()
      end
      if !enum_name
        matches = /\}\s?([a-zA-Z0-9_]+)?;/.match(line)
        name = matches[1]? || "" if matches
      end
      const_name = name.as(String)
      return NodeEnum.new(const_name, const_nodes)
    end
    def self.parse_eos(matches : Regex::MatchData, input : File) : Node
      case matches[1]
      when "enum"
        return self.parse_enum(matches, input)
      when "struct"
        return self.parse_struct(matches, input)
      else
        raise Exception.new("parse_eos")
      end
    end
    def self.parse_file(filename : String, writer) : Void
      raise Exception.new("File: #{filename} does not exist!") if !File.exists?(filename)
      input = File.new(filename)
      input.each_line do |line|
        line = Parser.shift_star(line)
        line = line.lstrip()
        # Ignore lines that begin with /**/ type comments
        if line.index(COMMENT_RGX) == 0
          next
        # Find an include, and its path and style
        elsif (matches = /#include\s*?("|<)([a-zA-Z0-9\-_$]+)/.match(line))
          writer.add(Parser.parse_include(matches, input))
        # Find a macro or a const
        elsif (matches = /#define\ ([a-zA-Z0-9_]+)?(?:\(([a-zA-Z0-9,]+)\))?\s(.*)/.match(line))
          writer.add(Parser.parse_macro_or_const(matches, input))
        # Match extern function decl
        elsif (matches = /extern\s(?:[a-zA-Z0-9]+)\s?(?:const)?\s?([a-zA-Z0-9_\*]+)?\s([a-zA-Z0-9_\*]+)\s([a-zA-Z0-9_]+)\(/.match(line))
          writer.add(Parser.parse_function(matches, line, input))
        # Match enum, struct, or struct X X;
        elsif (matches = /(?:typedef\s)?(enum|struct)\s?([a-zA-Z0-9_]+\**)?\s?(\**[a-zA-Z0-9_]+)?/.match(line))
          writer.add(Parser.parse_eos(matches, input))
        # Match non enum typedef
        elsif (matches = /(typedef\s*(.*))/.match(line))
          writer.add(Parser.parse_typedef(matches, line, input))
        end
      end
    end
  end
end