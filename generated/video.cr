# AUTOMATICALLY GENERATED, DO NOT EDIT
require "./SDL_stdinc"
require "./SDL_pixels"
require "./SDL_rect"
require "./SDL_surface"
require "./begin_code"
SDL_WINDOWPOS_UNDEFINED_MASK = 0x1FFF0000u32
macro SDL_WINDOWPOS_UNDEFINED_DISPLAY X
  {{(SDL_WINDOWPOS_UNDEFINED_MASK|(X))}}
end
SDL_WINDOWPOS_UNDEFINED = SDL_WINDOWPOS_UNDEFINED_DISPLAY(0)
macro SDL_WINDOWPOS_ISUNDEFINED X
  {{(((X)&0xFFFF0000) == SDL_WINDOWPOS_UNDEFINED_MASK)}}
end
SDL_WINDOWPOS_CENTERED_MASK = 0x2FFF0000u32
macro SDL_WINDOWPOS_CENTERED_DISPLAY X
  {{(SDL_WINDOWPOS_CENTERED_MASK|(X))}}
end
SDL_WINDOWPOS_CENTERED = SDL_WINDOWPOS_CENTERED_DISPLAY(0)
macro SDL_WINDOWPOS_ISCENTERED X
  {{(((X)&0xFFFF0000) == SDL_WINDOWPOS_CENTERED_MASK)}}
end
require "./close_code"
lib LibSDL
  struct DisplayMode
    format : UInt32
    w : Int32
    h : Int32
    refresh_rate : Int32
    driverdata : Void*
  end
  type Window = Void
  enum WindowFlags
    WINDOW_FULLSCREEN = 0x00000001,
    WINDOW_OPENGL = 0x00000002,
    WINDOW_SHOWN = 0x00000004,
    WINDOW_HIDDEN = 0x00000008,
    WINDOW_BORDERLESS = 0x00000010,
    WINDOW_RESIZABLE = 0x00000020,
    WINDOW_MINIMIZED = 0x00000040,
    WINDOW_MAXIMIZED = 0x00000080,
    WINDOW_INPUT_GRABBED = 0x00000100,
    WINDOW_INPUT_FOCUS = 0x00000200,
    WINDOW_MOUSE_FOCUS = 0x00000400,
    WINDOW_FULLSCREEN_DESKTOP = ( SDL_WINDOW_FULLSCREEN | 0x00001000 ),
    WINDOW_FOREIGN = 0x00000800,
    WINDOW_ALLOW_HIGHDPI = 0x00002000,
    WINDOW_MOUSE_CAPTURE = 0x00004000,
    WINDOW_ALWAYS_ON_TOP = 0x00008000,
    WINDOW_SKIP_TASKBAR = 0x00010000,
    WINDOW_UTILITY = 0x00020000,
    WINDOW_TOOLTIP = 0x00040000,
    WINDOW_POPUP_MENU = 0x00080000       ,
  end
  enum WindowEventID
    WINDOWEVENT_NONE,
    WINDOWEVENT_SHOWN,
    WINDOWEVENT_HIDDEN,
    WINDOWEVENT_EXPOSED,
    WINDOWEVENT_MOVED,
    WINDOWEVENT_RESIZED,
    WINDOWEVENT_SIZE_CHANGED,
    WINDOWEVENT_MINIMIZED,
    WINDOWEVENT_MAXIMIZED,
    WINDOWEVENT_RESTORED,
    WINDOWEVENT_ENTER,
    WINDOWEVENT_LEAVE,
    WINDOWEVENT_FOCUS_GAINED,
    WINDOWEVENT_FOCUS_LOST,
    WINDOWEVENT_CLOSE,
    WINDOWEVENT_TAKE_FOCUS,
    WINDOWEVENT_HIT_TEST,
  end
  type GLContext = Void*
  enum GLattr
    GL_RED_SIZE,
    GL_GREEN_SIZE,
    GL_BLUE_SIZE,
    GL_ALPHA_SIZE,
    GL_BUFFER_SIZE,
    GL_DOUBLEBUFFER,
    GL_DEPTH_SIZE,
    GL_STENCIL_SIZE,
    GL_ACCUM_RED_SIZE,
    GL_ACCUM_GREEN_SIZE,
    GL_ACCUM_BLUE_SIZE,
    GL_ACCUM_ALPHA_SIZE,
    GL_STEREO,
    GL_MULTISAMPLEBUFFERS,
    GL_MULTISAMPLESAMPLES,
    GL_ACCELERATED_VISUAL,
    GL_RETAINED_BACKING,
    GL_CONTEXT_MAJOR_VERSION,
    GL_CONTEXT_MINOR_VERSION,
    GL_CONTEXT_EGL,
    GL_CONTEXT_FLAGS,
    GL_CONTEXT_PROFILE_MASK,
    GL_SHARE_WITH_CURRENT_CONTEXT,
    GL_FRAMEBUFFER_SRGB_CAPABLE,
    GL_CONTEXT_RELEASE_BEHAVIOR,
  end
  enum GLprofile
    GL_CONTEXT_PROFILE_CORE = 0x0001,
    GL_CONTEXT_PROFILE_COMPATIBILITY = 0x0002,
    GL_CONTEXT_PROFILE_ES = 0x0004 ,
  end
  enum GLcontextFlag
    GL_CONTEXT_DEBUG_FLAG = 0x0001,
    GL_CONTEXT_FORWARD_COMPATIBLE_FLAG = 0x0002,
    GL_CONTEXT_ROBUST_ACCESS_FLAG = 0x0004,
    GL_CONTEXT_RESET_ISOLATION_FLAG = 0x0008,
  end
  enum GLcontextReleaseFlag
    GL_CONTEXT_RELEASE_BEHAVIOR_NONE = 0x0000,
    GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = 0x0001,
  end
  fun get_num_video_drivers = SDL_GetNumVideoDrivers() : Int32
  fun get_video_driver = SDL_GetVideoDriver(index : Int32) : UInt8*
  fun video_init = SDL_VideoInit(driver_name : UInt8*) : Int32
  fun video_quit = SDL_VideoQuit() : Void
  fun get_current_video_driver = SDL_GetCurrentVideoDriver() : UInt8*
  fun get_num_video_displays = SDL_GetNumVideoDisplays() : Int32
  fun get_display_name = SDL_GetDisplayName(display_index : Int32) : UInt8*
  fun get_display_bounds = SDL_GetDisplayBounds(display_index : Int32, rect : Rect*) : Int32
  fun get_display_dpi = SDL_GetDisplayDPI(display_index : Int32, ddpi : Float32*, hdpi : Float32*, vdpi : Float32*) : Int32
  fun get_display_usable_bounds = SDL_GetDisplayUsableBounds(display_index : Int32, rect : Rect*) : Int32
  fun get_num_display_modes = SDL_GetNumDisplayModes(display_index : Int32) : Int32
  fun get_display_mode = SDL_GetDisplayMode(display_index : Int32, mode_index : Int32, mode : DisplayMode*) : Int32
  fun get_desktop_display_mode = SDL_GetDesktopDisplayMode(display_index : Int32, mode : DisplayMode*) : Int32
  fun get_current_display_mode = SDL_GetCurrentDisplayMode(display_index : Int32, mode : DisplayMode*) : Int32
  fun get_closest_display_mode = SDL_GetClosestDisplayMode(display_index : Int32, mode : DisplayMode*, closest : DisplayMode*) : DisplayMode*
  fun get_window_display_index = SDL_GetWindowDisplayIndex(window : Window*) : Int32
  fun set_window_display_mode = SDL_SetWindowDisplayMode(window : Window*, mode : DisplayMode*) : Int32
  fun get_window_display_mode = SDL_GetWindowDisplayMode(window : Window*, mode : DisplayMode*) : Int32
  fun get_window_pixel_format = SDL_GetWindowPixelFormat(window : Window*) : UInt32
  fun create_window = SDL_CreateWindow(title : UInt8*, x : Int32, y : Int32, w : Int32, h : Int32, flags : UInt32) : Window*
  fun create_window_from = SDL_CreateWindowFrom(data : Void*) : Window*
  fun get_window_id = SDL_GetWindowID(window : Window*) : UInt32
  fun get_window_from_id = SDL_GetWindowFromID(id : UInt32) : Window*
  fun get_window_flags = SDL_GetWindowFlags(window : Window*) : UInt32
  fun set_window_title = SDL_SetWindowTitle(window : Window*, title : UInt8*) : Void
  fun get_window_title = SDL_GetWindowTitle(window : Window*) : UInt8*
  fun set_window_icon = SDL_SetWindowIcon(window : Window*, icon : Surface*) : Void
  fun set_window_data = SDL_SetWindowData(window : Window*, name : UInt8*, userdata : Void*) : Void*
  fun get_window_data = SDL_GetWindowData(window : Window*, name : UInt8*) : Void*
  fun set_window_position = SDL_SetWindowPosition(window : Window*, x : Int32, y : Int32) : Void
  fun get_window_position = SDL_GetWindowPosition(window : Window*, x : Int32*, y : Int32*) : Void
  fun set_window_size = SDL_SetWindowSize(window : Window*, w : Int32, h : Int32) : Void
  fun get_window_size = SDL_GetWindowSize(window : Window*, w : Int32*, h : Int32*) : Void
  fun get_window_borders_size = SDL_GetWindowBordersSize(window : Window*, top : Int32*, left : Int32*, bottom : Int32*, right : Int32*) : Int32
  fun set_window_minimum_size = SDL_SetWindowMinimumSize(window : Window*, min_w : Int32, min_h : Int32) : Void
  fun get_window_minimum_size = SDL_GetWindowMinimumSize(window : Window*, w : Int32*, h : Int32*) : Void
  fun set_window_maximum_size = SDL_SetWindowMaximumSize(window : Window*, max_w : Int32, max_h : Int32) : Void
  fun get_window_maximum_size = SDL_GetWindowMaximumSize(window : Window*, w : Int32*, h : Int32*) : Void
  fun set_window_bordered = SDL_SetWindowBordered(window : Window*, bordered : Int32) : Void
  fun set_window_resizable = SDL_SetWindowResizable(window : Window*, resizable : Int32) : Void
  fun show_window = SDL_ShowWindow(window : Window*) : Void
  fun hide_window = SDL_HideWindow(window : Window*) : Void
  fun raise_window = SDL_RaiseWindow(window : Window*) : Void
  fun maximize_window = SDL_MaximizeWindow(window : Window*) : Void
  fun minimize_window = SDL_MinimizeWindow(window : Window*) : Void
  fun restore_window = SDL_RestoreWindow(window : Window*) : Void
  fun set_window_fullscreen = SDL_SetWindowFullscreen(window : Window*, flags : UInt32) : Int32
  fun get_window_surface = SDL_GetWindowSurface(window : Window*) : Surface*
  fun update_window_surface = SDL_UpdateWindowSurface(window : Window*) : Int32
  fun update_window_surface_rects = SDL_UpdateWindowSurfaceRects(window : Window*, rects : Rect*, numrects : Int32) : Int32
  fun set_window_grab = SDL_SetWindowGrab(window : Window*, grabbed : Int32) : Void
  fun get_window_grab = SDL_GetWindowGrab(window : Window*) : Int32
  fun get_grabbed_window = SDL_GetGrabbedWindow() : Window*
  fun set_window_brightness = SDL_SetWindowBrightness(window : Window*, brightness : Float32) : Int32
  fun get_window_brightness = SDL_GetWindowBrightness(window : Window*) : Float32
  fun set_window_opacity = SDL_SetWindowOpacity(window : Window*, opacity : Float32) : Int32
  fun get_window_opacity = SDL_GetWindowOpacity(window : Window*, out_opacity : Float32*) : Int32
  fun set_window_modal_for = SDL_SetWindowModalFor(modal_window : Window*, parent_window : Window*) : Int32
  fun set_window_input_focus = SDL_SetWindowInputFocus(window : Window*) : Int32
  fun set_window_gamma_ramp = SDL_SetWindowGammaRamp(window : Window*, red : UInt16*, green : UInt16*, blue : UInt16*) : Int32
  fun get_window_gamma_ramp = SDL_GetWindowGammaRamp(window : Window*, red : UInt16*, green : UInt16*, blue : UInt16*) : Int32
  enum HitTestResult
    HITTEST_NORMAL,
    HITTEST_DRAGGABLE,
    HITTEST_RESIZE_TOPLEFT,
    HITTEST_RESIZE_TOP,
    HITTEST_RESIZE_TOPRIGHT,
    HITTEST_RESIZE_RIGHT,
    HITTEST_RESIZE_BOTTOMRIGHT,
    HITTEST_RESIZE_BOTTOM,
    HITTEST_RESIZE_BOTTOMLEFT,
    HITTEST_RESIZE_LEFT,
  end
  type HitTest = (Window*, Point*, Void*) -> HitTestResult
  fun set_window_hit_test = SDL_SetWindowHitTest(window : Window*, callback : HitTest, callback_data : Void*) : Int32
  fun destroy_window = SDL_DestroyWindow(window : Window*) : Void
  fun is_screen_saver_enabled = SDL_IsScreenSaverEnabled() : Int32
  fun enable_screen_saver = SDL_EnableScreenSaver() : Void
  fun disable_screen_saver = SDL_DisableScreenSaver() : Void
  fun gl_load_library = SDL_GL_LoadLibrary(path : UInt8*) : Int32
  fun gl_get_proc_address = SDL_GL_GetProcAddress(proc : UInt8*) : Void*
  fun gl_unload_library = SDL_GL_UnloadLibrary() : Void
  fun gl_extension_supported = SDL_GL_ExtensionSupported(extension : UInt8*) : Int32
  fun gl_reset_attributes = SDL_GL_ResetAttributes() : Void
  fun gl_set_attribute = SDL_GL_SetAttribute(attr : GLattr, value : Int32) : Int32
  fun gl_get_attribute = SDL_GL_GetAttribute(attr : GLattr, value : Int32*) : Int32
  fun gl_create_context = SDL_GL_CreateContext(window : Window*) : GLContext
  fun gl_make_current = SDL_GL_MakeCurrent(window : Window*, context : GLContext) : Int32
  fun gl_get_current_window = SDL_GL_GetCurrentWindow() : Window*
  fun gl_get_current_context = SDL_GL_GetCurrentContext() : GLContext
  fun gl_get_drawable_size = SDL_GL_GetDrawableSize(window : Window*, w : Int32*, h : Int32*) : Void
  fun gl_set_swap_interval = SDL_GL_SetSwapInterval(interval : Int32) : Int32
  fun gl_get_swap_interval = SDL_GL_GetSwapInterval() : Int32
  fun gl_swap_window = SDL_GL_SwapWindow(window : Window*) : Void
  fun gl_delete_context = SDL_GL_DeleteContext(context : GLContext) : Void
end
