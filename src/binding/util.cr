module Bindings
    class Util
        def self.rm_prefix(prefix : String, str : String) : String
            if str.starts_with?(prefix)
                return str[prefix.size..-1]
            end
            return str
        end
    end
end