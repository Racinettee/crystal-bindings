require "./util"

module Bindings
    abstract class Node
        abstract def emit(context, output : File) : Void
        def self.conv_type(input)
            conv = {
                "int" => "Int32", "Sint32" => "Int32", "Uint32" => "UInt32",
                "int*" => "Int32*", "Sint32*" => "Int32*", "Uint32*" => "UInt32*",
                "Sint16" => "Int16", "Sint16*" => "Int16*", "Uint16" => "UInt16", "Uint16*" => "UInt16*",
                "char*" => "UInt8*",
                "void" => "Void", "void*" => "Void*",
                "float" => "Float32", "float*" => "Float32*",
                "bool" => "Int32"
            }[input]?
            return conv || input
        end
    end
    class NodeVariable < Node
        def initialize(@typ : String, @name : String)
        end
        def emit(context, output : File) : Void
            typ = Node.conv_type(Util.rm_prefix(context["rm_prefix"].as(String), @typ))
            if context["type_only"]?
                output.print("#{typ}")
            else
                output.print("#{@name.underscore.downcase} : #{typ}")
            end
        end
    end
    class NodeFunction < Node
        def initialize(@typ : String, @name : String, @args : Array(NodeVariable))
        end
        def emit(context, output : File) : Void
            name = Util.rm_prefix(context["rm_prefix"].as(String), @name)
            output.print("  fun #{name.underscore.downcase} = #{@name}(")
            @args.each do |arg|
                arg.emit(context, output)
                output.print(", ") if arg != @args.last
            end
            typ = Node.conv_type(Util.rm_prefix(context["rm_prefix"].as(String), @typ))
            output.puts(") : #{typ}")
        end
    end
    class NodeInclude < Node
        # Style being one of < or "
        def initialize(@path : String, @style : String)
        end
        def emit(context, output : File) : Void
            relative = @style == "\"" ? "./" : ""
            output.puts("require \"#{relative}#{@path}\"")
        end
    end
    class NodeConst < Node
        def initialize(@name : String, @value : String?)
        end
        def emit(context, output : File) : Void
            name = @name
            
            if name != name.upcase
                return
            end

            is_enum = context["block"]? == "enum"
            
            if is_enum
                output.print("    ")
                name = Util.rm_prefix(context["rm_prefix"].as(String), @name)
            end

            if @value
                value = @value.as(String)
                value += "32" if value.ends_with?("u")
                output.print("#{name} = #{value}")
            else
                # This case is likely to only run when
                # invoked from NodeEnum#emit
                output.print("#{name}")
            end
            output.puts(is_enum ? "," : "")
        end
    end
    class NodeMacro < Node
        # Expect args to already be sorted out into an array
        def initialize(@name : String, @args : String|Nil, @expression : String)
        end
        def emit(context, output : File) : Void
            output.puts(
            "macro #{@name} #{@args}\n"+
            "  {{#{@expression}}}\n"+
            "end")
        end
    end
    class NodeEnum < Node
        def initialize(@name : String, @consts : Array(NodeConst))
        end
        def emit(context, output : File) : Void
            puts @name
            output.puts("  enum #{Util.rm_prefix(context["rm_prefix"].as(String), @name)}")
            context["block"] = "enum"
            @consts.each do |const|
                const.emit(context, output)
            end
            context["block"] = ""
            output.puts("  end")
        end
    end
    class NodeStruct < Node
        def initialize(@name : String, @mems : Array(NodeVariable))
        end
        def emit(context, output : File) : Void
            output.puts("  struct #{Util.rm_prefix(context["rm_prefix"].as(String), @name)}")
            @mems.each do |mem|
                output.print("    ")
                mem.emit(context, output)
                output.puts
            end
            output.puts("  end")
        end
    end
    class NodeTypedef < Node
        def initialize(@name : String, @type : String)
        end
        def emit(context, output : File) : Void
            typ = (@type == @name ? "void" : @type)
            output.puts("  type #{Util.rm_prefix(context["rm_prefix"].as(String), @name)} = #{Node.conv_type(typ)}")
        end
    end
    class NodeFunctionTypedef < Node
        def initialize(@name : String, @result : String, @args : Array(NodeVariable))
        end
        def emit(context, output : File) : Void
            name = Util.rm_prefix(context["rm_prefix"].as(String), @name)
            result = Node.conv_type(Util.rm_prefix(context["rm_prefix"].as(String), @result))
            output.print("  type #{name} = (")
            context["type_only"] = true
            @args.each do |arg|
                arg.emit(context, output)
                output.print(", ") if arg != @args.last
            end
            context["type_only"] = false
            output.print(") -> ")
            output.puts("#{result}")
        end
    end
    class NodeGlobal < Node
        def initialize()
            @nodes = [] of Node
        end
        def add(node : Node) : Void
            @nodes << node
        end
        def emit(context, output : File) : Void
            @nodes.each do |node|
                node.emit(context, output)
            end
        end
    end
    class NodeLib < Node
        def initialize(@name : String)
            @nodes = [] of Node
        end
        def add(node : Node) : Void
            @nodes << node
        end
        def emit(context, output : File) : Void
            output.puts "lib #{@name}"
            @nodes.each do |node|
                node.emit(context, output)
            end
            output.puts "end"
        end
    end
end
