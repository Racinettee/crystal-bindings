require "option_parser"
require "./binding/*"

module Bindings
  begin
    files = [] of String
    lib_name = ""
    strip_prefix = ""
    outdir = ""
    OptionParser.parse! do |parser|
      parser.banner = "Usage: bindings --lib name --files files..."
      parser.on("-l LIB", "--lib LIB", "Add @[Link(name)] to the first file output") do |libname|
        lib_name = libname
      end
      parser.on("-f FILES,", "--files FILES,", "Space seperated list of files") do |file_names|
        files = file_names.split(',')
      end
      parser.on("-sprf PREFIX", "--strip-prefix PREFIX", "Prefix to strip from functions, types and constants and the filename") do |prefix|
        strip_prefix = prefix
      end
      parser.on("-d OUTDIR", "--out-dir OUTDIR", "Output directory, CWD by default") do |out_dir|
        outdir = out_dir
      end
      parser.on("-h", "--help", "Display this message") do 
        puts parser
      end
    end
    outdir = Dir.current() if outdir == ""
    Dir.mkdir(outdir) if !Dir.exists?(outdir)
    puts outdir

    files.each do |filename|
      context = {} of String => String|Bool|Int32|Nil
      context["filename"] = filename
      context["rm_prefix"] = strip_prefix
      context["outdir"] = outdir
      context["link_lib"] = filename == files.first
      context["lib_name"] = lib_name
      writer = WriteOuter.new("Lib#{lib_name}", strip_prefix)
      writer.gen_file(context)
    end
  rescue exception : Exception
    puts exception.message
  end
end
